import sys, json
from flask import Flask, render_template, request, url_for, redirect, json, jsonify, Response, make_response
from flask_sqlalchemy import SQLAlchemy
import requests
from random import randint
from sqlalchemy import desc, or_
sys.path.append("modules")
import gitlabAPI
from database import app, database, Message, Sender, Category

# ------------
# index
# ------------
@app.route('/')
def index():
    return render_template('index.html')
#
# ------------
# emails
# ------------	
@app.route('/emails/')
def emails():
    email_list = database.session.query(Message)#.all()
    sort = request.args.get("getattr")
    asc = request.args.get("asc") #ascending
    #SORTING
    if sort is not None:
        if asc == "asc":
            email_list = email_list.order_by(getattr(Message, sort)).all()
        else:
            email_list = email_list.order_by(desc(getattr(Message, sort))).all()
    #SEARCHING
    search_info = request.args.get("input")
    if search_info is not None:
        if search_info == '':
            email_list = email_list
        else:
            search_info = search_info.split()
            email_list = search_messages(search_info)
    return render_template('emails.html', email_list = email_list)

#HELPER FUNCTION
def search_messages(terms):
    search_result = []
    for term in terms:
        queries = []
        queries.append(Message.msg_id.contains(term))
        queries.append(Message.subject.contains(term))
        queries.append(Message.sender.contains(term))
        messages = Message.query.filter(or_(*queries))
        for email in messages:
            search_result.append(email)
    return search_result


# ------------
# labels
# ------------	    
@app.route('/labels/')
def labels():
    label_list = database.session.query(Category)#.all()
    sort = request.args.get("getattr")
    asc = request.args.get("asc") #ascending
    #SORTING
    if sort is not None:
        if asc == "asc":
            label_list = label_list.order_by(getattr(Category, sort)).all()
        else:
            label_list = label_list.order_by(desc(getattr(Category, sort))).all()
    #SEARCHING
    search_info = request.args.get("input")
    if search_info is not None:
        if search_info == '':
            label_list = label_list
        else:
            search_info = search_info.split()
            label_list = search_labels(search_info)
    return render_template('labels.html', label_list = label_list)

#HELPER FUNCTION
def search_labels(terms):
    search_result = []
    for term in terms:
        queries = []
        queries.append(Category.label.contains(term))
        queries.append(Category.top_sender.contains(term))
        label_list = Category.query.filter(or_(*queries))
        for label in label_list:
            search_result.append(label)
    return search_result
    
# ------------
# senders
# ------------	
@app.route('/senders/')
def senders():
    sender_list = database.session.query(Sender)#.all()
    #SORTING
    sort = request.args.get("getattr")
    asc = request.args.get("asc") #ascending
    if sort is not None:
        if asc == "asc":
            sender_list = sender_list.order_by(getattr(Sender, sort)).all()
        else:
            sender_list = sender_list.order_by(desc(getattr(Sender, sort))).all()
    search_info = request.args.get("input")
    if search_info is not None:
        if search_info == '':
            sender_list = sender_list
        else:
            search_info = search_info.split()
            sender_list = search_senders(search_info)
    return render_template('Senders.html', sender_list = sender_list)

#HELPER FUNCTION
def search_senders(terms):
    search_result = []
    for term in terms:
        queries = []
        queries.append(Sender.email.contains(term))
        queries.append(Sender.name.contains(term))
        queries.append(Sender.domain.contains(term))
        senders_list = Sender.query.filter(or_(*queries))
        for sender in senders_list:
            search_result.append(sender)
    return search_result

# ------------
# oneEmail
# ------------	
@app.route('/oneEmail/<instance>', methods = ['GET', 'POST'])
def oneEmail(instance):
    email_list = database.session.query(Message).all()
    for email in email_list:
        if str(email) == str(instance):
            instance = email
    return render_template('oneEmail.html', email_instance = instance)

# ------------
# oneEmail
# ------------	
@app.route('/oneLabel/<instance>') # category
def oneLabel(instance):
    label_list = database.session.query(Category).all()
    for label in label_list:
        string = str(label).split()[1][:len(str(label).split()[1])-1]
        if string == str(instance):
            instance = label
    return render_template('oneLabel.html', label_instance = instance)

# ------------
# oneEmail
# ------------	
@app.route('/oneSender/<instance>') #email_instance.sender
def oneSender(instance):
    sender_list = database.session.query(Sender).all()
    for sender in sender_list:
        string = str(sender).split()[1][:len(str(sender).split()[1])-1]
        if string == str(instance):
            instance = sender

    return render_template('oneSender.html', sender_instance = instance)

# ------------
# sort
# ------------	
@app.route('/sort/')
def sort():
    return render_template('sort.html')

# ------------
# about
# ------------	
@app.route("/about/")
def about():
    b = open('bios.txt', 'r')
    bios = b.read().splitlines()
    b.close()
    issues = gitlabAPI.get_issues()
    commits = gitlabAPI.get_commits()
    tot_commits = len(gitlabAPI.commits)
    tot_issues = len(gitlabAPI.issues)
    return render_template("about.html", bios = bios, commits = commits, tot_com = tot_commits, tot_issues = tot_issues, issues = issues)

@app.route('/json/')
def json():
    senders = database.session.query(Sender).all()
    with open("data.json", "w", encoding="utf-8") as file:
        json.dump(senders, file, indent = 4)
        return render_template('Senders.html')

# ----------------
#   interacting with POSTMAN
# -------------


@app.route('/emails-API/') 
def viewMessage(): 

    emails = Message.query.all() 

    response = list()
    for email in emails:   
        response.append({ 
            "msg_id" : email.msg_id,
            "datetime" : email.datetime,
            "subject" : email.subject,
            "sender" : email.sender,
            "unread" : email.unread,
            "starred" : email.starred,
            "important" : email.important,
            "category" : email.category,
            "size" : email.size

        }) 

    return make_response({ 
        'emails': response
    }, 200) 


@app.route('/senders-API/') 
def viewSender(): 

    users = Sender.query.all() 

    response = list()
    for user in users:   
        response.append({ 
            "email" : user.email,
            "name" : user.name,
            "domain" : user.domain,
            "message_count" : user.message_count,
            "total_size" : user.total_size,
            "top_label" : user.top_label
        }) 

    return make_response({ 
        'senders': response
    }, 200) 

@app.route('/labels-API/') 
def viewLabel(): 

    tags = Category.query.all() 

    response = list()
    for tag in tags:   
        response.append({ 
            "label" : tag.label,
            "sender_count" : tag.sender_count,
            "message_count" : tag.message_count,
            "top_sender" : tag.top_sender,
            "total_size" : tag.total_size
        }) 

    return make_response({ 
        'labels': response
    }, 200) 

@app.route('/emails-API/add/', methods =['GET', 'POST']) 
def addMessage():  

    if request.method == 'POST':
    
        data = request.json
        
        msg_id = data['msg_id']
        datetime = data['datetime']
        subject = data['subject']
        sender = data['sender']
        unread = data['unread']
        starred = data['starred']
        important = data['important']
        category = data['category']
        size = data['size']
        
        # checking if email already exists 
        email = Message.query.filter_by(msg_id = msg_id).first() 

        if not email: 
            try: 
                # creating new Message
                email = Message( 
                    msg_id = msg_id,
                    datetime = datetime,
                    subject = subject,
                    sender = sender,
                    unread = unread,
                    starred = starred,
                    important = important,
                    category = category,
                    size = size
                ) 

                database.session.add(email) 
                database.session.commit() 
                # response 
                responseObject = { 
                    'status' : 'success'
                } 

                return make_response(responseObject, 200) 
            except Exception as e: 
                responseObject = { 
                    'Exception' : str(e),
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
            
        else: 

            responseObject = { 
                'status' : 'fail', 
                'message': 'Message already exists'
            } 

            return make_response(responseObject, 403) 
    else:
        email = Message.query.filter_by(msg_id = msg_id).first()
        if not email:
        
            responseObject = { 
                'status' : 'fail'
            }
        else:
            responseObject = { 
                'status' : 'success'
            }
        return make_response(responseObject, 400)
        

@app.route('/senders-API/add/', methods =['GET', 'POST']) 
def addSender():  

    if request.method == 'POST':
    
        data = request.json
        
        email = data['email']
        name = data['name']
        domain = data['domain']
        message_count = data['message_count']
        total_size = data['total_size']
        messages = data['messages']

        
        # checking if user already exists 
        user = Sender.query.filter_by(email = email).first() 

        if not user: 
            try: 
                # creating new sender
                user = Sender( 
                    email = email,
                    name = name,
                    domain = domain,
                    message_count = message_count,
                    total_size = total_size,
                    messages = messages
                ) 

                database.session.add(user) 
                database.session.commit() 
                # response 
                responseObject = { 
                    'status' : 'success'
                } 

                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
            
        else: 

            responseObject = { 
                'status' : 'fail', 
                'message': 'Message already exists'
            } 

            return make_response(responseObject, 403) 
    else:
        user = Sender.query.filter_by(email = email).first()
        if not user:
        
            responseObject = { 
                'status' : 'fail'
            }
        else:
            responseObject = { 
                'status' : 'success'
            }
        return make_response(responseObject, 400)
        


@app.route('/labels-API/add/', methods =['GET', 'POST']) 
def addLabel():  

    if request.method == 'POST':
    
        data = request.json
        
        label = data['label']
        sender_count = data['sender_count']
        message_count = data['message_count']
        top_sender = data['top_sender']
        total_size = data['total_size']
        messages = data['messages']

        
        # checking if label already exists 
        tag = Category.query.filter_by(label = label).first() 

        if not tag: 
            try: 
                # creating new label
                tag = Category( 
                    label = label,
                    sender_count = sender_count,
                    message_count = message_count,
                    top_sender = top_sender,
                    total_size = total_size,
                    messages = messages
                ) 

                database.session.add(tag) 
                database.session.commit() 
                # response 
                responseObject = { 
                    'status' : 'success'
                } 

                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
            
        else: 

            responseObject = { 
                'status' : 'fail', 
                'message': 'Label already exists'
            } 

            return make_response(responseObject, 403) 
    else:
        tag = Category.query.filter_by(label = label).first()
        if not tag:
        
            responseObject = { 
                'status' : 'fail'
            }
        else:
            responseObject = { 
                'status' : 'success'
            }
        return make_response(responseObject, 400)
        

@app.route('/emails-API/delete/', methods =['GET', 'POST']) 
def deleteMessage(): 

    if request.method == 'POST':
    
        data = request.json
        msg_id = data['msg_id']
        email = Message.query.filter_by(msg_id = msg_id).first() 

        if email: 
            try: 
                database.session.delete(email) 
                database.session.commit() 
                # response 
                responseObject = { 
                    'status' : 'success'
                } 

                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
            
        else: 

            responseObject = { 
                'status' : 'fail', 
                'message': 'Email does not exist'
            } 

            return make_response(responseObject, 403) 
            
    else:
        email = Message.query.filter_by(msg_id = msg_id).first()
        if not email:
        
            responseObject = { 
                'status' : 'fail'
            }
        else:
            responseObject = { 
                'status' : 'success'
            }
        return make_response(responseObject, 400)
        
@app.route('/senders-API/delete/', methods =['GET', 'POST']) 
def deleteSender(): 

    if request.method == 'POST':
    
        data = request.json
        email = data['email']
        user = Sender.query.filter_by(email = email).first() 

        if user: 
            try: 
                database.session.delete(user) 
                database.session.commit() 
                # response 
                responseObject = { 
                    'status' : 'success'
                } 

                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
            
        else: 

            responseObject = { 
                'status' : 'fail', 
                'message': 'Sender does not exist'
            } 

            return make_response(responseObject, 403)    
    else:
        user = Sender.query.filter_by(email = email).first()
        if not user:
        
            responseObject = { 
                'status' : 'fail'
            }
        else:
            responseObject = { 
                'status' : 'success'
            }
        return make_response(responseObject, 400)
        
@app.route('/labels-API/delete/', methods =['GET', 'POST']) 
def deleteLabel(): 

    if request.method == 'POST':
    
        data = request.json
        label = data['label']
        tag = Category.query.filter_by(label = label).first() 

        if tag: 
            try: 
                database.session.delete(tag) 
                database.session.commit() 
                # response 
                responseObject = { 
                    'status' : 'success'
                } 

                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
            
        else: 

            responseObject = { 
                'status' : 'fail', 
                'message': 'Label does not exist'
            } 

            return make_response(responseObject, 403) 
    else:
        tag = Category.query.filter_by(label = label).first()
        if not tag:
        
            responseObject = { 
                'status' : 'fail'
            }
        else:
            responseObject = { 
                'status' : 'success'
            }
        return make_response(responseObject, 400)

# debug=True activates the automatic reloader. Therefore, if you use it, make sure to add "db.drop_all()"
# right before "db.create_all()" in "models.py".
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
