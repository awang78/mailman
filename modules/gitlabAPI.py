import gitlab
from collections import defaultdict

gl = gitlab.Gitlab(url = "https://gitlab.com", private_token = "glpat-cnb-g7-ac-FJsvYEU5zF")
project = gl.projects.get(42928795)
issues = list(project.issues.list(iterator = True))
commits = list(project.commits.list(iterator = True))
#don't have more than 20 users
users = list(project.users.list())

#returns a dictionary of users
def __userdict():
    user_dict = defaultdict(int)
    for user in users:
        user_dict.update({user.name:0})
    return user_dict

def get_commits():
    user_dict = __userdict()
    for commit in commits:
        user_dict[commit.author_name] += 1
    return user_dict

def get_issues():
    user_dict = __userdict()
    for issue in issues:
        user_dict[issue.author['name']] += 1
    return user_dict
    