import gmail
from models import app, database, Message, Sender, Category
import sys
sys.path.append("..")

def sync_database():
    user, categories, senders, messages = gmail.sync()
    
    category_db_objects = {}
    for label in categories:
        size_count = 0
        message_count = 0
        for message in messages:
            if messages[message]["category"] == label:
                size_count += messages[message]["size"]
                message_count += 1
        senders_list = []
        for message in messages:
            if messages[message]["category"] == label:
                senders_list.append(messages[message]["sender_email"])
        if any(senders_list):
            top_sender = max(set(senders_list), key = senders_list.count)
        else:
            top_sender = "NONE"
        sender_count = len(set(senders_list))
        entry = Category(label = label, sender_count = sender_count, message_count = message_count, top_sender = top_sender, total_size = size_count)
        category_db_objects[label] = entry
        
        # After creating the category, we can then add it to the session.
        database.session.add(entry)
        database.session.commit()
    
    sender_db_object = {}
    for address in senders:
        count = 0
        memory_count = 0
        labels_list = []
        for message in messages:
            if messages[message]["sender_email"] == address:
                memory_count += messages[message]["size"]
                count += 1
                labels_list.append(messages[message]["category"])
        if any(labels_list):
            top_label = max(set(labels_list), key = labels_list.count)
        else:
            top_label = "NONE"
        entry = Sender(email = address, name = senders[address]["name"], domain = senders[address]["domain"], total_size = memory_count, message_count = count, top_label=top_label)
        sender_db_object[address] = entry
        
        # After creating the category, we can then add it to the session.
        database.session.add(entry)
        database.session.commit()
    
    for message in messages:
        msg_id = message
        datetime = messages[message]["datetime"]
        subject = messages[message]["subject"]
        
        address = messages[message]["sender_email"]
        sender = sender_db_object[address].email
        
        unread = messages[message]["unread"]
        starred = messages[message]["starred"]
        important = messages[message]["important"]
        category = messages[message]["category"]
        size = messages[message]["size"]
        
        entry = Message(msg_id = msg_id, datetime = datetime, subject = subject, sender = sender, unread = unread, starred = starred, important = important, category = category, size = size)
        
        # After creating the category, we can then add it to the session.
        database.session.add(entry)
        database.session.commit()

with app.app_context():
    sync_database()
