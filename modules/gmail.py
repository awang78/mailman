import os.path, re, json
from datetime import datetime

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# If modifying these scopes, delete the file token.json.
SCOPES = ["https://www.googleapis.com/auth/gmail.metadata",
          "https://www.googleapis.com/auth/gmail.labels"]

def connect():
    """Establishes a connection to the Gmail API,
    and returns a service.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    
    if os.path.exists("token.json"):
        creds = Credentials.from_authorized_user_file("token.json", SCOPES)
    
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                "credentials.json", SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open("token.json", "w") as token:
            token.write(creds.to_json())
    
    try:
        # Call the Gmail API
        service = build("gmail", "v1", credentials=creds)
        return service
    
    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f"An error occurred: {error}")

def get_current_user(service):
    try:
        user = service.users().getProfile(userId="me").execute()
        return user
    
    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f"An error occurred: {error}")

def get_messages(service):
    """Returns a dictionary of indexes of all messages
    in the inbox as "id" and "threadId" pairs.
    """
    try:
        results = service.users().messages().list(maxResults=80, userId="me").execute()
        index_pairs = results.get("messages", [])
        return index_pairs
    
    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f"An error occurred: {error}")

def get_message_metadata(service, index_pair):
    """Fetches message's metadata,
    and returns a Mailman message object
    with the following properties:
    datetime     (datetime object)
    subject      (string)
    sender_name  (string)
    sender_email (string)
    labels       (list)
    size         (integer)
    """
    msg_id = index_pair["id"]
    thr_id = index_pair["threadId"]
    format_type = "metadata"
    header_type = ["Date", "Subject", "From"]
    
    try:
        retreived_data = service.users().messages().get(userId="me", id=msg_id, format=format_type, metadataHeaders=header_type).execute()

        # Estimated size in bytes
        size = retreived_data["sizeEstimate"]
        size = int(size)
        
        # internalDate is in epoch milliseconds
        epoch_time = retreived_data["internalDate"]
        epoch_time = int(epoch_time) / 1000
        datetime_object = datetime.fromtimestamp(epoch_time)

        # Read status, starring, importance, category
        labels = retreived_data["labelIds"]
        unread = "UNREAD" in labels
        starred = "STARRED" in labels
        important = "IMPORTANT" in labels
        category = "Uncategorized"
        for label in labels:
            if label == "CATEGORY_FORUMS":
                category = "Forums"
            if label == "CATEGORY_UPDATES":
                category = "Updates"
            if label == "CATEGORY_PERSONAL":
                category = "Personal"
            if label == "CATEGORY_PROMOTIONS":
                category = "Promotions"
            if label == "CATEGORY_SOCIAL":
                category = "Social"
        
        headers = retreived_data["payload"]["headers"]
        for header in headers:
            # Conforming string case before comparisons,
            # because some developers are stupid
            key = header["name"].capitalize()
            if key == "Subject":
                subject = header["value"]
            if key == "From":
                address = header["value"]
                components = re.split("([^+]+)\s\<([\S@.]+)\>", address)

                # Name and email address supplied, in compliance
                if len(components) > 1:
                    name = components[1]
                    email = components[2]
                # Only email address is supplied, not in compliance
                else:
                    name = "Unnamed"
                    email = components[0][1:-1]

                email_parts = re.split("@", email)
                local = email_parts[0]
                domain = email_parts[1]
                
        message = {"datetime": datetime_object,
                   "subject": subject,
                   "sender_name": name,
                   "sender_email": email,
                   "sender_domain": domain,
                   "unread": unread,
                   "starred": starred,
                   "important": important,
                   "category": category,
                   "size": size}
        return message
    
    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f"An error occurred: {error}")

def get_thread_metadata(service, index_pair):
    """Fetches thread's metadata,
    and returns a Mailman thread object.
    """
    pass

def get_labels(service):
    """Fetches user's labels,
    and returns a labels object.
    """
    try:
        results = service.users().labels().list(userId="me").execute()
        labels = results.get("labels", [])
        return labels
    
    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f"An error occurred: {error}")

def sync():
    service = connect()
    user = get_current_user(service)
    index_pairs = get_messages(service)

    categories = {"Forums": [],
                  "Updates": [],
                  "Personal": [],
                  "Promotions": [],
                  "Social": [],
                  "Uncategorized": []}
    senders = {}
    messages = {}
    
    for pair in index_pairs:
        index = pair["id"]
        message = (get_message_metadata(service, pair))

        # Append messages dictionary
        messages[index] = message

        # Append senders dictionary
        address = message["sender_email"]
        if address in senders:
            senders[address]["messages"].append(index)
        else:
            senders[address] = {"name": message["sender_name"],
                                "domain": message["sender_domain"],
                                "messages": [index]}

        # Append categories dictionary
        category = message["category"]
        categories[category].append(index)

##    with open("categories.json", "w", encoding="utf-8") as file:
##        json.dump(categories, file, indent = 4)
##    with open("senders.json", "w", encoding="utf-8") as file:
##        json.dump(senders, file, indent = 4)
##    with open("messages.json", "w", encoding="utf-8") as file:
##        json.dump(messages, file, indent = 4)
    
    return user, categories, senders, messages

def main():
    """Test drives the Gmail API
    """
    print("Connecting to GMail API.")
    service = connect()
    
    print("Requesting user's profile")
    user = get_current_user(service)    
    
    print("Requesting user's labels")
    labels = get_labels(service)
    
    if not labels:
        print("No labels found.")
        return
    print("Labels:")
    for label in labels:
        print("—", label["name"])
    
    print()
    
    print("Requesting user's last 500 emails")
    
    index_pairs = get_messages(service)
    print("Success getting indexes.")
    
    messages = []
    for pair in index_pairs:
        messages.append(get_message_metadata(service, pair))
    print("Success fetching messages.\n")
            
if __name__ == "__main__":
    main()
