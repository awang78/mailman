import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, template_folder = '../templates', static_folder='../static')
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DB_STRING",'postgresql://postgres:cs331e@localhost:5432/emailsdb')
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
#app.config["SQLALCHEMY_ENABLE_POOL_PRE_PING"] = True
database = SQLAlchemy(app)

class Message(database.Model):
    __tablename__ = "message"

    msg_id = database.Column(database.Text, primary_key = True)
    datetime = database.Column(database.DateTime, nullable = False)
    subject = database.Column(database.Text, nullable = False)
    sender = database.Column(database.String(128), database.ForeignKey("sender.email"))
    unread = database.Column(database.Boolean, nullable = False)
    starred = database.Column(database.Boolean, nullable = False)
    important = database.Column(database.Boolean, nullable = False)
    category = database.Column(database.String(16), database.ForeignKey("category.label"))
    size = database.Column(database.Integer, nullable = False)

class Sender(database.Model):
    __tablename__ = "sender"

    email = database.Column(database.String(128), primary_key = True)
    name = database.Column(database.String(128), nullable = False)
    domain = database.Column(database.String(128), nullable = False)
    message_count = database.Column(database.Integer, nullable = False)
    total_size = database.Column(database.Integer, nullable = False)
    top_label = database.Column(database.String(16), database.ForeignKey("category.label"))
    messages = database.relationship("Message", backref = "msg_sender")

class Category(database.Model):
    __tablename__ = "category"

    label = database.Column(database.String(16), primary_key = True)
    sender_count = database.Column(database.Integer, nullable = False)
    message_count = database.Column(database.Integer, nullable = False)
    top_sender = database.Column(database.Text, nullable = False)
    total_size = database.Column(database.Integer, nullable = False)
    messages = database.relationship("Message", backref = "msg_category")

with app.app_context():
    database.drop_all()
    database.create_all()
