from io import StringIO
import unittest
import sys
import main
sys.path.append("modules")
from models import database, Message, Sender, Category
class Test (unittest.TestCase):
    def setUp(self):
        main.app.config["TESTING"] = True
        self.client = main.app.test_client()

    def testIndex(self):
        with self.client:
            response = self.client.get("/")
            self.assertEqual(response.status_code, 200)
            
    def testEmails(self):
        with self.client:
            response = self.client.get("/emails/")
            self.assertEqual(response.status_code, 200)
    
    def testLabels(self):
        with self.client:
            response = self.client.get("/labels/")
            self.assertEqual(response.status_code, 200)
            
    def testSenders(self):
        with self.client:
            response = self.client.get("/senders/")
            self.assertEqual(response.status_code, 200)
            
    def testAbout(self):
        with self.client:
            response = self.client.get("/about/")
            self.assertEqual(response.status_code, 200)
            
    def testEmailsAPI(self):
        with self.client:
            response = self.client.get("/emails-API/")
            self.assertEqual(response.status_code, 200)
            
    def testSendersAPI(self):
        with self.client:
            response = self.client.get("/senders-API/")
            self.assertEqual(response.status_code, 200)
            
    def testLabelsAPI(self):
        with self.client:
            response = self.client.get("/labels-API/")
            self.assertEqual(response.status_code, 200)

    #test reading/deleting from database
            # test adding to database
    def test_insert_senders(self):
        with main.app.app_context():
            s = Sender(email = "LebronJames@gmail.com", name = "LebronJames", domain = "gmail.com", message_count = '20', total_size = '1000')
            database.session.add(s)
            database.session.commit()
            exists = database.session.query(Sender).filter_by(email = "LebronJames@gmail.com").first() is not None
            self.assertTrue(exists)
    
    def test_insert_message(self):
        with main.app.app_context():
            s = Sender(email = "NotLebronJames@gmail.com", name = "NotLebronJames", domain = "Notgmail.com", message_count = '21', total_size = '1001')
            database.session.add(s)
            database.session.commit()
            m = Message(msg_id = "18706sa12353f", datetime = '2023-03-10 10:30:30.0000', subject = "Taco Tuesday", sender = "NotLebronJames@gmail.com", unread = True, starred = True, important = True, category = "ball", size = 1000)
            database.session.add(m)
            database.session.commit()
            exists = database.session.query(Message).filter_by(msg_id = '18706sa12353f').first() is not None
            self.assertTrue(exists)
        
    def test_insert_category(self):
        with main.app.app_context():
            s = Category(label = "ball", sender_count = '20', message_count = '20', top_sender = "LebronJames@gmail.com", total_size = '125000')
            database.session.add(s)
            database.session.commit()
            exists = database.session.query(Category).filter_by(label = "ball").first() is not None
            self.assertTrue(exists)
    
    def test_read_senders(self):
        with main.app.app_context():
            q = database.session.query(Sender).filter_by(email = "LebronJames@gmail.com").one()
            self.assertEqual(str(q.email), 'LebronJames@gmail.com')
            self.assertEqual(str(q.name), 'LebronJames')
            self.assertEqual(str(q.domain), 'gmail.com')
            self.assertEqual(str(q.message_count), '20')
            self.assertEqual(str(q.total_size), '1000')
        
    def test_read_message(self):
        with main.app.app_context():
            q = database.session.query(Message).filter_by(msg_id = "18706sa12353f").one()
            self.assertEqual(str(q.datetime), '2023-03-10 10:30:30')
            self.assertEqual(str(q.subject), 'Taco Tuesday')
            self.assertEqual(str(q.sender), 'NotLebronJames@gmail.com')
            self.assertEqual(str(q.unread), 'True')
            self.assertEqual(str(q.starred), 'True')
            self.assertEqual(str(q.important), 'True')
            self.assertEqual(str(q.category), 'ball')
            self.assertEqual(str(q.size), '1000')
        
    def test_read_labels(self):
        with main.app.app_context():
            q = database.session.query(Category).filter_by(label = "ball").one()
            self.assertEqual(str(q.label), 'ball')
            self.assertEqual(str(q.sender_count), '20')
            self.assertEqual(str(q.message_count), '20')
            self.assertEqual(str(q.top_sender), "LebronJames@gmail.com")
            self.assertEqual(str(q.total_size), '125000')
            
    #more site tests
    def testOneMessage(self):
        with self.client:
            response = self.client.get("/oneEmail/18706sa12353f")
            self.assertEqual(response.status_code, 200)
            
    def testOneSender(self):
        with self.client:
            response = self.client.get("/oneSender/LebronJames@gmail.com")
            self.assertEqual(response.status_code, 200)
            
    def testOneLabel(self):
        with self.client:
            response = self.client.get("/oneLabel/ball")
            self.assertEqual(response.status_code, 200)
            

if __name__ == "__main__":
    unittest.main()
